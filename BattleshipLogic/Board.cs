﻿using BattleshipLogic.Internal;

namespace BattleshipLogic
{
    public class Board
    {
        private BoardData data;

        public Board(int rows, int cols) => data = BoardOperations.Create(rows, cols);

        /// <summary>
        /// Number of rows.
        /// </summary>
        public int Rows => data.Rows;

        /// <summary>
        /// Number of columns.
        /// </summary>
        public int Cols => data.Cols;

        public CellState this[int row, int col] => data.Cells[row, col];

        /// <summary>
        /// True if there are any ship cells left on the board.
        /// </summary>
        public bool HasShipsLeft => BoardOperations.HasShipsLeft(data);

        /// <summary>
        /// Attacks the cell at the given coordinates.
        /// </summary>
        public bool Attack(int row, int col)
        {
            data = BoardOperations.Attack(data, new Vector(row, col));
            return data.Cells[row, col] == CellState.Hit;
        }

        /// <summary>
        /// Adds a ship of the given length and orientation at the specified coordinates. 
        /// The coordinates refer to the top left cell of the ship.
        /// </summary>
        public bool AddShip(int row, int col, int len, ShipOrientation o)
        {
            Vector pos = new Vector(row, col);
            if (!BoardOperations.CanAddShip(data, pos, len, o)) return false;
            data = BoardOperations.AddShip(data, pos, len, o);
            return true;
        }
    }
}
