﻿namespace BattleshipLogic
{
    public enum CellState
    {
        Empty,
        Ship,
        Hit,
        Miss
    }
}
