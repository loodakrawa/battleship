﻿namespace BattleshipLogic.Internal
{
    internal class BoardData
    {
        public readonly int Rows;
        public readonly int Cols;
        public readonly CellState[,] Cells;

        public BoardData(CellState[,] cells)
        {
            Rows = cells.GetLength(0);
            Cols = cells.GetLength(1);
            Cells = cells;
        }
    }
}
