﻿using System;

namespace BattleshipLogic.Internal
{
    // Functions to check or change the state of the board. 
    // All the functions are pure, i.e. the modified state is returned as a new copy.
    internal static class BoardOperations
    {
        public static BoardData Create(int rows, int cols) => new BoardData(new CellState[rows, cols]);

        public static bool HasShipsLeft(BoardData board)
        {
            for (int row = 0; row < board.Rows; ++row)
            {
                for (int col = 0; col < board.Cols; ++col) if (board.Cells[row, col] == CellState.Ship) return true;
            }
            return false;
        }

        public static BoardData Attack(BoardData board, Vector pos)
        {
            if (!Contains(board, pos)) throw new ArgumentException($"Position is outside the board: {pos}");

            BoardData updated = Clone(board);
            if (updated.Cells[pos.X, pos.Y] == CellState.Ship) updated.Cells[pos.X, pos.Y] = CellState.Hit;
            else updated.Cells[pos.X, pos.Y] = CellState.Miss;

            return updated;
        }

        public static BoardData AddShip(BoardData board, Vector topLeft, int len, ShipOrientation o)
        {
            if (len <= 0) throw new ArgumentException("Ship length must be greater than 0");
            if (!CanAddShip(board, topLeft, len, o)) throw new ArgumentException($"Can't add ship: {len} {o} @ {topLeft}");

            BoardData updated = Clone(board);
            Vector bottomRight = topLeft + GetShipSize(len, o);

            for (int row = topLeft.X; row < bottomRight.X; ++row)
            {
                for (int col = topLeft.Y; col < bottomRight.Y; ++col) updated.Cells[row, col] = CellState.Ship;
            }

            return updated;
        }

        public static bool CanAddShip(BoardData board, Vector topLeft, int len, ShipOrientation o)
        {
            if (len <= 0 || !Contains(board, topLeft)) return false;

            Vector bottomRight = topLeft + GetShipSize(len, o);
            if (!Contains(board, bottomRight)) return false;

            for (int row = topLeft.X; row < bottomRight.X; ++row)
            {
                for (int col = topLeft.Y; col < bottomRight.Y; ++col)
                {
                    if (board.Cells[row, col] != CellState.Empty) return false;
                }
            }

            return true;
        }

        private static BoardData Clone(BoardData data) => new BoardData((CellState[,])data.Cells.Clone());

        private static Vector GetShipSize(int len, ShipOrientation o) => o == ShipOrientation.Horizontal ? new Vector(1, len) : new Vector(len, 1);

        private static bool Contains(BoardData board, Vector pos) => pos.X >= 0 && pos.X < board.Rows && pos.Y >= 0 && pos.Y < board.Cols;
    }
}
