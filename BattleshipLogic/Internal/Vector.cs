﻿namespace BattleshipLogic.Internal
{
    internal struct Vector
    {
        public readonly int X;
        public readonly int Y;

        public Vector(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Vector operator +(Vector a, Vector b) => new Vector(a.X + b.X, a.Y + b.Y);

        public override string ToString() => $"[{X},{Y}]";
    }
}
