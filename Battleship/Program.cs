﻿using BattleshipLogic;
using System;

namespace Battleship
{
    class Program
    {
        static void Main(string[] args)
        {
            Board board = new Board(10, 10);
            GenerateShips(board, 5);
            Print(board);

            while (board.HasShipsLeft)
            {
                Console.Write("Enter Coordinates: ");
                string input = Console.ReadLine();

                if (input == "black sheep wall") Print(board, true);
                if (input == "exit") break;

                if (!TryParseCoordiantes(board, input, out (int Row, int Col) c)) continue;

                board.Attack(c.Row, c.Col);
                Print(board);
            }
        }

        private static void GenerateShips(Board board, int ships)
        {
            Random random = new Random();

            int shipCount = 0;

            while (shipCount < ships)
            {
                int row = random.Next(0, board.Rows);
                int col = random.Next(0, board.Cols);
                int len = random.Next(1, 5);
                ShipOrientation o = random.Next() % 2 == 0 ? ShipOrientation.Horizontal : ShipOrientation.Vertical;

                if (board.AddShip(row, col, len, o)) ++shipCount;
            }
        }

        private static bool TryParseCoordiantes(Board board, string input, out (int, int) coordinates)
        {
            coordinates = (0, 0);

            if (input.Length < 2) return false;

            char first = input[0];
            int col = first - 'A';
            if (col < 0 || col >= board.Cols) return false;

            string second = input.Substring(1, input.Length - 1);
            if (!int.TryParse(second, out int row)) return false;
            row -= 1;
            if (row < 0 || row >= board.Cols) return false;

            coordinates = (row, col);
            return true;
        }

        private static void Print(Board board, bool showShips = false)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("  ");
            for (int col = 0; col < board.Cols; ++col)
            {
                Console.Write((char)('A' + col));
                if (col < board.Cols - 1) Console.Write(" ");
            }
            Console.ResetColor();

            Console.WriteLine();

            for (int row = 0; row < board.Rows; ++row)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(row + 1);
                Console.Write(" ");
                Console.ResetColor();

                for (int col = 0; col < board.Cols; ++col)
                {
                    CellState state = board[row, col];
                    Console.ForegroundColor = GetColour(state);
                    Console.Write(GetSymbol(state, showShips));
                    if (col < board.Cols - 1) Console.Write(" ");
                }
                if (row < board.Rows - 1) Console.WriteLine();
            }

            Console.WriteLine();
            Console.WriteLine();
        }

        private static ConsoleColor GetColour(CellState state)
        {
            switch (state)
            {
                case CellState.Empty: return ConsoleColor.White;
                case CellState.Ship: return ConsoleColor.Blue;
                case CellState.Hit: return ConsoleColor.Red;
                case CellState.Miss: return ConsoleColor.Cyan;
                default: return ConsoleColor.White;
            }
        }

        private static string GetSymbol(CellState state, bool showShips)
        {
            switch (state)
            {
                case CellState.Empty: return " ";
                case CellState.Ship: return showShips ? "H" : " ";
                case CellState.Hit: return "X";
                case CellState.Miss: return "O";
                default: return " ";
            }
        }
    }
}
